#!/usr/bin/env python3

import json
from collections import defaultdict
import MySQLdb

dbname = ''
dbuser = ''
dbpass = ''
ofile = 'verselist.txt'


def get_verses():
  _verses = []
  #sql = "SELECT chapter c, verse v, translation t from bgslokas.slokas"
  db = MySQLdb.connect(db=dbname, user=dbuser, passwd=dbpass)
  cursor = db.cursor()
  sql = "SELECT * from bgslokas.slokas ORDER BY chapter, verse ASC"
  cursor.execute(sql)

  _verses = cursor.fetchall()
  cursor.close()
  db.close()
  return _verses

def write_verses_flat(data):
  verses = get_verses()
  verse_file = open(ofile, 'w')
  for verse in verses:
    strVerse = ''.join(['\'Chapter ', str(verse[0]), ', Verse ', verse[1], ': ', verse[2], '\',\n'])
    print (strVerse)
    verse_file.write(strVerse)

  verse_file.close()
  print ('done writing ', ofile)

def write_verses_dict(data):
  verses = get_verses()
  _obj = defaultdict(defaultdict)
  for verse in verses:
    _obj[verse[0]][verse[1]] = verse[2]

  verse_file = open(ofile, 'w')
  verse_file.write(json.dumps(_obj))
  verse_file.close()
  print ('done writing ', ofile)


if __name__ == '__main__':

  ### ToDo
  # add ability to pull parameter to specify which file you want...

  data = get_verses()
  #write_verses_flat(data)
  write_verses_dict(data)
