rm index.zip
cd src
zip -rX ../index.zip *
cd ..
aws lambda update-function-code --function-name BGVerses_AlexaPollyReadBG --zip-file fileb://index.zip