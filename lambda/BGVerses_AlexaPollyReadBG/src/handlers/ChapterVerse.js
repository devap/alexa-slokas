const languageStrings = require('./languageStrings.js');

module.exports =  {
  'GetChapterVerseIntent': function () {
  console.log('GetChapterVerseIntent()');
  const chapterSlot = this.event.request.intent.slots.Chapter;
  const verseSlot = this.event.request.intent.slots.Verse;
  let chapterName;
  let verseName;
  if (chapterSlot && chapterSlot.value) {
    chapterName = chapterSlot.value.toLowerCase();
  }
  if (verseSlot && verseSlot.value) {
    verseName = verseSlot.value.toLowerCase();
  }

  const verses = this.t('CHAPTER_VERSES');
  const chapter = verses[chapterName]
  const verse = verses[chapterName][verseName];

  if (verse && chapter) {
    let speechOutput = "Chapter " + chapterName;
        speechOutput += " Verse " + verseName;
        speechOutput += ' ' + verse;

    this.attributes.speechOutput = speechOutput;

    console.log('speechOutput(): ' + speechOutput );

    this.response.speak(this.attributes.speechOutput)
    this.emit(':responseReady');
  }

  if (!chapter) {

    let speechOutput = this.t('CHAPTER_NOT_FOUND_MESSAGE');
    const repromptSpeech = this.t('CHAPTER_NOT_FOUND_REPROMPT');

    if (chapterName) {
      speechOutput += this.t('CHAPTER_NOT_FOUND_WITH_ITEM_NAME', chapterName);
    } else {
      speechOutput += this.t('CHAPTER_NOT_FOUND_WITHOUT_ITEM_NAME');
    }

    this.attributes.speechOutput = speechOutput;
    this.attributes.repromptSpeech = repromptSpeech;

    console.log('speechOutput(): ' + speechOutput );

    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    this.emit(':responseReady');

    } else if (!verse) {

    let speechOutput = this.t('VERSE_NOT_FOUND_MESSAGE');
    const repromptSpeech = this.t('VERSE_NOT_FOUND_REPROMPT');

    if (verseName) {
      speechOutput += this.t('VERSE_NOT_FOUND_WITH_ITEM_NAME', verseName);
    } else {
      speechOutput += this.t('VERSE_NOT_FOUND_WITHOUT_ITEM_NAME');
    }

    this.attributes.speechOutput = speechOutput;
    this.attributes.repromptSpeech = repromptSpeech;

    console.log('speechOutput(): ' + speechOutput );

    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    this.emit(':responseReady');
    }

  },

}
