const languageStrings = require('./languageStrings.js');

module.exports =  {
  'handleChapterVerseIntentAllSlotsAreFilled': function() {
    console.log('handleChapterVerseIntentAllSlotsAreFilled()');
    const chapterSlot = this.event.request.intent.slots.Chapter;
    const verseSlot = this.event.request.intent.slots.Verse;
    let chapterName;
    let verseName;
    if (chapterSlot && chapterSlot.value) {
      chapterName = chapterSlot.value.toLowerCase();
    }
    if (verseSlot && verseSlot.value) {
      verseName = verseSlot.value.toLowerCase();
    }

    //const cardTitle = this.t('DISPLAY_CARD_TITLE', this.t('SKILL_NAME'), itemName);
    const verses = this.t('CHAPTER_VERSES');
    const chapter = verses[chapterName]
    const verse = verses[chapterName][verseName];

    if (verse && chapter) {
      let speechOutput = "Chapter " + chapterName;
          speechOutput += " Verse " + verseName;
          speechOutput += ' ' + verse;

      this.attributes.speechOutput = speechOutput;
      this.attributes.repromptSpeech = this.t('VERSE_REPEAT_MESSAGE');

      console.log('speechOutput(): ' + speechOutput );

      this.response.speak(this.attributes.speechOutput)
                   .listen(this.attributes.repromptSpeech);
      this.emit(':responseReady');
    }

    if (!chapter) {

      let speechOutput = this.t('CHAPTER_NOT_FOUND_MESSAGE');
      const repromptSpeech = this.t('CHAPTER_NOT_FOUND_REPROMPT');

      if (chapterName) {
        speechOutput += this.t('CHAPTER_NOT_FOUND_WITH_ITEM_NAME', chapterName);
      } else {
        speechOutput += this.t('CHAPTER_NOT_FOUND_WITHOUT_ITEM_NAME');
      }

      this.attributes.speechOutput = speechOutput;
      this.attributes.repromptSpeech = repromptSpeech;

      console.log('speechOutput(): ' + speechOutput );

      this.response.speak(this.attributes.speechOutput)
                   .listen(this.attributes.repromptSpeech);
      this.emit(':responseReady');

    } else if (!verse) {

      let speechOutput = this.t('VERSE_NOT_FOUND_MESSAGE');
      const repromptSpeech = this.t('VERSE_NOT_FOUND_REPROMPT');

      if (verseName) {
        speechOutput += this.t('VERSE_NOT_FOUND_WITH_ITEM_NAME', verseName);
      } else {
        speechOutput += this.t('VERSE_NOT_FOUND_WITHOUT_ITEM_NAME');
      }

      this.attributes.speechOutput = speechOutput;
      this.attributes.repromptSpeech = repromptSpeech;

      console.log('speechOutput(): ' + speechOutput );

      this.response.speak(this.attributes.speechOutput)
                   .listen(this.attributes.repromptSpeech);
      this.emit(':responseReady');
    }
  },

  'GetVerseToReadIntent': function () {
      console.log('GetVerseToReadIntent()');
      const intentObj = this.event.request.intent;
      if (!intentObj.slots.Chapter.value) {
          console.log('missing intentObj.slots.Chapter.value');
          const slotToElicit = 'Chapter';
          const speechOutput = 'Which Chapter?';
          const repromptSpeech = speechOutput;
          this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech);
      } else if (!intentObj.slots.Verse.value) {
          console.log('missing intentObj.slots.Verse.value');
          const slotToElicit = 'Verse';
          const speechOutput = 'Which Verse?';
          const repromptSpeech = speechOutput;
          const updatedIntent = intentObj;
          // An intent object representing the intent sent to your skill.
          // You can use this property set or change slot values and confirmation status if necessary.
          this.emit(':elicitSlot', slotToElicit, speechOutput, repromptSpeech, updatedIntent);
      } else {
          console.log('SUCCESS: handing over to: handleChapterVerseIntentAllSlotsAreFilled()');
          this.handleChapterVerseIntentAllSlotsAreFilled();
      }
    },

};
