const chapter_verses = require('../data/chapter_verses.js');

module.exports = {

  'en': {
    translation: {
      CHAPTER_VERSES: chapter_verses.verses,
      SKILL_NAME: 'As It Is',
      WELCOME_MESSAGE: "Welcome to %s. You can ask for a verse such as, 'Read a verse' ... Now, what can I help you with?",
      WELCOME_REPROMPT: 'For instructions on what you can say, please say help me.',
      DISPLAY_CARD_TITLE: '%s  - Verse for %s.',
      HELP_MESSAGE: "Learn everything you can about the Bhagavad-gita. You can start by saying As It Is Read a verse' or you can say exit...Now, what can I help you with?",
      HELP_REPROMPT: "You can ask for a verse such as, 'Read a verse' , or you can say exit...Now, what can I help you with?",
      STOP_MESSAGE: 'Goodbye!',
      CHAPTER_REPEAT_MESSAGE: 'Try saying repeat.',
      CHAPTER_NOT_FOUND_MESSAGE: "I\'m sorry, I currently do not know ",
      CHAPTER_NOT_FOUND_WITH_ITEM_NAME: 'the chapter for %s. ',
      CHAPTER_NOT_FOUND_WITHOUT_ITEM_NAME: 'that chapter. ',
      CHAPTER_NOT_FOUND_REPROMPT: 'What else can I help with?',

      VERSE_REPEAT_MESSAGE: 'Try saying repeat.',
      VERSE_NOT_FOUND_MESSAGE: "I\'m sorry, I currently do not know ",
      VERSE_NOT_FOUND_WITH_ITEM_NAME: 'the verse for %s. ',
      VERSE_NOT_FOUND_WITHOUT_ITEM_NAME: 'that verse. ',
      VERSE_NOT_FOUND_REPROMPT: 'What else can I help with?',
    }
  }

};
