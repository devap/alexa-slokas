const languageStrings = require('./languageStrings.js');

module.exports =  {

  'AMAZON.HelpIntent': function () {
    this.attributes.speechOutput = this.t('HELP_MESSAGE');
    this.attributes.repromptSpeech = this.t('HELP_REPROMPT');

    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    this.emit(':responseReady');
  },

  'AMAZON.RepeatIntent': function () {
    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    this.emit(':responseReady');
  },

  'AMAZON.StopIntent': function () {
    let myName = '';
    if (this.attributes['name']) {
      myName = this.attributes['name'];
    }

    const speechOutput = 'Goodbye ' + myName;
    this.response.speak(speechOutput);
    this.emit(':responseReady');
  },

  'AMAZON.CancelIntent': function () {
    let myName = '';
    if (this.attributes['name']) {
      myName = this.attributes['name'];
    }

    const speechOutput = 'Goodbye ' + myName;
    this.response.speak(speechOutput);
    this.emit(':responseReady');
  },

  'SessionEndedRequest': function () {
    console.log(`Session ended: ${this.event.request.reason}`);
    this.attributes['endedSessionCount'] += 1;
  },

  'Unhandled': function () {
    this.attributes.speechOutput = this.t('HELP_MESSAGE');
    this.attributes.repromptSpeech = this.t('HELP_REPROMPT');
    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    this.emit(':responseReady');
   },

};
