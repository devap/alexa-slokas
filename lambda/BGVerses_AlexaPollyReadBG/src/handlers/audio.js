const audio_verses = require('../data/audio_verses.js');
const dravida_complete = require('../data/dravida_complete.js');
//const dravida_purports = require('../data/dravida_purports.js');
const dravida_translations = require('../data/dravida_translations.js');

const languageStrings = require('./languageStrings.js');

module.exports = {

  'GetAudioVerseIntent': function() {
    console.log('GetAudioVerseIntent()');

    let speechOutput = 'OK, here is your verse: ';
    const behavior = 'REPLACE_ALL';

    let i = Math.floor(Math.random() * audio_verses.length);
    const url = encodeURI(audio_verses[i]);

    const token = 'BG-AsItIs';
    const expectedPreviousToken = 'expectedPreviousStream';
    const offsetInMilliseconds = 10000;

    console.log('speechOutput(): ' + speechOutput);

    this.attributes.speechOutput = speechOutput;
    this.response.speak(this.attributes.speechOutput)
      .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },

  'GetAudioChaptersIntent': function() {
    console.log('GetAudioChaptersIntent()');
    const chapterSlot = this.event.request.intent.slots.Chapter;
    let chapterName;
    if (chapterSlot && chapterSlot.value) {
      chapterName = chapterSlot.value.toLowerCase();
    }

    let _obj = dravida_complete["verses"];
    let track = "";
    Object.keys(_obj).forEach(function(chapter) {
      if (_obj[chapter]["chapter"] == chapterName) {
        track = _obj[chapter]["track"];
      }
    });

    let speechOutput = 'OK, here is';
    speechOutput += ' Chapter ' + chapterName;
    speechOutput += ' by Dravida Prabhu ';

    const behavior = 'REPLACE_ALL';
    const prefix = dravida_complete["prefix"];
    const url = encodeURI(prefix + track);
    const token = 'BG-AsItIs';
    const expectedPreviousToken = 'expectedPreviousStream';
    const offsetInMilliseconds = 10000;

    console.log('speechOutput(): ' + speechOutput);
    console.log('url: ' + url);

    this.attributes.speechOutput = speechOutput;
    this.response.shouldEndSession = true;
    this.response.speak(this.attributes.speechOutput)
                 .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds);
    this.emit(':responseReady');
  },

  'GetAudioTranslationsIntent': function() {
    console.log('GetAudioTranslationsIntent()');
    console.log('this.event.request.intent.slots: ' + this.event.request.intent.slots );
    const chapterSlot = this.event.request.intent.slots.Part;
    let chapterName;
    if (chapterSlot && chapterSlot.value) {
      chapterName = chapterSlot.value.toLowerCase();
    }

    let _obj = dravida_translations["verses"];
    let track = "";
    Object.keys(_obj).forEach(function(chapter) {
      if (_obj[chapter]["part"] == chapterName) {
        track = _obj[chapter]["track"];
      }
    });

    let speechOutput = 'OK, here is';
    speechOutput += " Part " + chapterName;
    speechOutput += " by Dravida Praboo ";

    const behavior = 'REPLACE_ALL';
    const prefix = dravida_translations["prefix"];
    const url = encodeURI(prefix + track);
    const token = 'BG-AsItIs';
    const expectedPreviousToken = 'expectedPreviousStream';
    const offsetInMilliseconds = 10000;

    console.log('speechOutput(): ' + speechOutput);
    console.log('url: ' + url);

    this.attributes.speechOutput = speechOutput;
    this.response.speak(this.attributes.speechOutput)
                 .audioPlayerPlay(behavior, url, token, expectedPreviousToken, offsetInMilliseconds)
                 .shouldEndSession = true;
    this.emit(':responseReady');
  }

};
