module.exports =  {

  'AudioPlayer.PlaybackStarted' : function() {
    console.log('Alexa begins playing the audio stream');
  },
  'AudioPlayer.PlaybackFinished' : function() {
    console.log('The stream comes to an end');
  },
  'AudioPlayer.PlaybackStopped' : function() {
    console.log('Alexa stops playing the audio stream');
  },
  'AudioPlayer.PlaybackNearlyFinished' : function() {
    console.log('The currently playing stream is nearly complate and the device is ready to receive a new stream');
  },
  'AudioPlayer.PlaybackFailed' : function() {
    console.log('Alexa encounters an error when attempting to play a stream');
  },

};
