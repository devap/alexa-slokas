module.exports =  {
  "prefix": "https://s3.amazonaws.com/polly-verses-bg-audio/Dravida/",
  "verses": [
    {
      "chapter": "0",
      "track": "Bhagavad-gita Complete/00 BGCOMP Introduction.mp3"
    },
    {
      "chapter": "1",
      "track": "Bhagavad-gita Complete/01 BGCOMP Ch 1.mp3"
    },
    {
      "chapter": "2",
      "track": "Bhagavad-gita Complete/02 BGCOMP Ch 2.mp3"
    },
    {
      "chapter": "3",
      "track": "Bhagavad-gita Complete/03 BGCOMP Ch 3.mp3"
    },
    {
      "chapter": "4",
      "track": "Bhagavad-gita Complete/04 BGCOMP Ch 4.mp3"
    },
    {
      "chapter": "5",
      "track": "Bhagavad-gita Complete/05 BGCOMP Ch 5.mp3"
    },
    {
      "chapter": "6",
      "track": "Bhagavad-gita Complete/06 BGCOMP Ch 6.mp3"
    },
    {
      "chapter": "7",
      "track": "Bhagavad-gita Complete/07 BGCOMP Ch 7.mp3"
    },
    {
      "chapter": "8",
      "track": "Bhagavad-gita Complete/08 BGCOMP Ch 8.mp3"
    },
    {
      "chapter": "9",
      "track": "Bhagavad-gita Complete/09 BGCOMP Ch 9.mp3"
    },
    {
      "chapter": "10",
      "track": "Bhagavad-gita Complete/10 BGCOMP Ch 10.mp3"
    },
    {
      "chapter": "11",
      "track": "Bhagavad-gita Complete/11 BGCOMP Ch 11.mp3"
    },
    {
      "chapter": "12",
      "track": "Bhagavad-gita Complete/12 BGCOMP Ch 12.mp3"
    },
    {
      "chapter": "13",
      "track": "Bhagavad-gita Complete/13 BGCOMP Ch 13.mp3"
    },
    {
      "chapter": "14",
      "track": "Bhagavad-gita Complete/14 BGCOMP Ch 14.mp3"
    },
    {
      "chapter": "15",
      "track": "Bhagavad-gita Complete/15 BGCOMP Ch 15.mp3"
    },
    {
      "chapter": "16",
      "track": "Bhagavad-gita Complete/16 BGCOMP Ch 16.mp3"
    },
    {
      "chapter": "17",
      "track": "Bhagavad-gita Complete/17 BGCOMP Ch 17.mp3"
    },
    {
      "chapter": "18",
      "track": "Bhagavad-gita Complete/18 BGCOMP Ch 18.mp3"
    }
  ]
}
