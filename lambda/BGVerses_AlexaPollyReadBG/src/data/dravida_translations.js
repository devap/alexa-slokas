module.exports =  {
  "prefix": "https://s3.amazonaws.com/polly-verses-bg-audio/Dravida/",
  "verses": [
    {
      "part": "1",
      "track": "Bhagavad-gita Translation/01 BGTRANS Introduction -- 2.14.mp3"
    },
    {
      "part": "2",
      "track": "Bhagavad-gita Translation/02 BGTRANS 2.15 -- 3.16.mp3"
    },
    {
      "part": "3",
      "track": "Bhagavad-gita Translation/03 BGTRANS 3.17 -- 5.29.mp3"
    },
    {
      "part": "4",
      "track": "Bhagavad-gita Translation/04 BGTRANS 6.1 -- 8.23.mp3"
    },
    {
      "part": "5",
      "track": "Bhagavad-gita Translation/05 BGTRANS 8.24 -- 11.24.mp3"
    },
    {
      "part": "6",
      "track": "Bhagavad-gita Translation/06 BGTRANS 11.25 -- 14.14.mp3"
    },
    {
      "part": "7",
      "track": "Bhagavad-gita Translation/07 BGTRANS 14.15 -- 18.16.mp3"
    },
    {
      "part": "8",
      "track": "Bhagavad-gita Translation/08 BGTRANS 18.17 -- 18.78.mp3"
    }
  ]
}
