module.exports =  {
  "prefix": "https://s3.amazonaws.com/polly-verses-bg-audio/Dravida/",
  "verses": [
    "Bhagavad-gita Purports/01 BGCOMP Introduction.mp3",
    "Bhagavad-gita Purports/02 BGCOMP Intro -- 1.1 purp.mp3",
    "Bhagavad-gita Purports/03 BGCOMP 1.1 purp -- 1.39.mp3",
    "Bhagavad-gita Purports/04 BGCOMP 1.40 -- 2.13 purp.mp3",
    "Bhagavad-gita Purports/05 BGCOMP 2.13 purp -- 2.29.mp3",
    "Bhagavad-gita Purports/06 BGCOMP 2.30 -- 2.51 purp.mp3",
    "Bhagavad-gita Purports/07 BGCOMP 2.51 purp -- 3.3 purp.mp3",
    "Bhagavad-gita Purports/08 BGCOMP 3.3 purp -- 3.23.mp3",
    "Bhagavad-gita Purports/09 BGCOMP 3.24 -- 4.1 purp.mp3",
    "Bhagavad-gita Purports/10 BGCOMP 4.1 purp -- 4.12 purp.mp3",
    "Bhagavad-gita Purports/11 BGCOMP 4.12 purp -- 4.32.mp3",
    "Bhagavad-gita Purports/12 BGCOMP 4.33  -- 5.12.mp3",
    "Bhagavad-gita Purports/13 BGCOMP 5.13 -- 6.3.mp3",
    "Bhagavad-gita Purports/14 BGCOMP 6.4 -- 6.27 purp.mp3",
    "Bhagavad-gita Purports/15 BGCOMP 6.27 purp -- 6.47 purp.mp3",
    "Bhagavad-gita Purports/16 BGCOMP 6.47 purp -- 7.15 purp.mp3",
    "Bhagavad-gita Purports/17 BGCOMP 7.15 purp -- 7.26 purp.mp3",
    "Bhagavad-gita Purports/18 BGCOMP 7.26 purp -- 8.15.mp3",
    "Bhagavad-gita Purports/19 BGCOMP 8.16 -- 9.3 purp.mp3",
    "Bhagavad-gita Purports/20 BGCOMP 9.3 purp -- 9.18 purp.mp3",
    "Bhagavad-gita Purports/21 BGCOMP 9.18 purp -- 10.1 purp.mp3",
    "Bhagavad-gita Purports/22 BGCOMP 10.1 purp -- 10.14 purp.mp3",
    "Bhagavad-gita Purports/23 BGCOMP 10.14 purp -- 11.1 purp.mp3",
    "Bhagavad-gita Purports/24 BGCOMP 11.1 purp -- 11.43 purp.mp3",
    "Bhagavad-gita Purports/25 BGCOMP 11.43 purp -- 12.1 purp.mp3",
    "Bhagavad-gita Purports/26 BGCOMP 12.1 purp -- 13.3 purp.mp3",
    "Bhagavad-gita Purports/27 BGCOMP 13.3 purp -- 13.19 purp.mp3",
    "Bhagavad-gita Purports/28 BGCOMP 13.19 purp -- 14.8 purp.mp3",
    "Bhagavad-gita Purports/29 BGCOMP 14.8 purp -- 15.4 purp.mp3",
    "Bhagavad-gita Purports/30 BGCOMP 15.4 purp -- 15.20.mp3",
    "Bhagavad-gita Purports/31 BGCOMP 16.1 -- 16.20 purp.mp3",
    "Bhagavad-gita Purports/32 BGCOMP 16.20 purp -- 17.23.mp3",
    "Bhagavad-gita Purports/33 BGCOMP 17.24 -- 18.33.mp3",
    "Bhagavad-gita Purports/34 BGCOMP 18.34 -- 18.65.mp3",
    "Bhagavad-gita Purports/35 BGCOMP 18.66 -- 18.73 purp.mp3",
    "Bhagavad-gita Purports/36 BGCOMP 18.73 purp -- 18.78.mp3"
  ]
}
