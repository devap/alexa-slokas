const Alexa = require('alexa-sdk');
require('./handlers/audio_utils.js');
const languageStrings = require('./handlers/languageStrings.js');

const audio_handlers = require('./handlers/audio.js');
const amazon_handlers = require('./handlers/amazon.js');
const dialog_handlers = require('./handlers/dialog.js');
const ChapterVerse = require('./handlers/ChapterVerse.js');

const all_verses = require('./data/all_verses.js');
const my_verses = require('./data/my_verses.js');
const study_verses = require('./data/study_verses.js');

const handlers = {
  'LaunchRequest': function () {
    console.log('LaunchRequest()->Bhagavad-gita As It Is Session Launched');
    let speechOutput = this.t('WELCOME_MESSAGE', this.t('SKILL_NAME'));

    this.attributes.speechOutput = speechOutput;
    this.attributes.repromptSpeech = this.t('WELCOME_REPROMPT');

    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    console.log('this.attributes.speechOutput: ' + this.attributes.speechOutput );
    this.emit(':responseReady');
  },

  'GetVerseIntent': function () {
    console.log('GetVerseIntent()');
    let speechOutput = 'Your verse for today is: ';
        speechOutput += getRandomVerse();

    console.log('speechOutput(): ' + speechOutput );

    this.attributes.speechOutput = speechOutput;
    this.response.speak(this.attributes.speechOutput);
    this.emit(':responseReady');
  },

  'GetMyVerseIntent': function () {
    console.log('GetMyVerseIntent()');
    let speechOutput = 'Hello Deva! Let\'s recite to the Bhagavad-gita!';
        speechOutput += getMyRandomVerse();

    console.log('speechOutput: ' + this.attributes.speechOutput );

    this.attributes.speechOutput = speechOutput;
    this.emit(':responseReady');
  },

  'GetStudyVerseIntent': function () {
    console.log('GetStudyVerseIntent()');
    let speechOutput = 'Here\s a verse from the Bhakti Sastri Exam: ';
        speechOutput += getRandomStudyVerse();

    console.log('speechOutput: ' + speechOutput );

    this.attributes.speechOutput = speechOutput;
    this.emit(':responseReady');
  },

};

//  helper functions  ===================================================================
function getRandomVerse() {
  //  '<audio src=\"" />\'',
  let newVerse = randomPhrase(all_verses.verses);
  return newVerse;
}

function getMyRandomVerse() {
  let newVerse = randomPhrase(my_verses.verses);
  return newVerse;
}

function getRandomStudyVerse() {
  //  '<audio src=\"" />\'',
  let newVerse = randomPhrase(study_verses.verses);
  return newVerse;
}

function randomPhrase(array) {
  // the argument is an array [] of words or phrases
  let i = 0;
  i = Math.floor(Math.random() * array.length);
  return(array[i]);
}

exports.handler = function(event, context, callback) {
  let alexa = Alexa.handler(event, context, callback);
  alexa.appId = 'amzn1.ask.skill.640a95ca-25d0-45fb-97b0-e5e9ba1a154e';
  alexa.dynamoDBTableName = 'AlexaAsItIs'; // for future use...
  alexa.resources = languageStrings;
  alexa.registerHandlers(handlers,
                         amazon_handlers,
                         audio_handlers,
                         dialog_handlers,
                         ChapterVerse
                        );
  alexa.execute();
};

