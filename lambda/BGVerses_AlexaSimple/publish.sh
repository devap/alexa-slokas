rm index.zip
cd src
zip -rX ../index.zip *
cd ..
aws lambda update-function-code --function-name BGVerses_AlexaSimple --zip-file fileb://index.zip
