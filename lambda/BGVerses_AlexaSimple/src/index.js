const Alexa = require('alexa-sdk');
require('./handlers/audio_utils.js');
const languageStrings = require('./handlers/languageStrings.js');

const amazon_handlers = require('./handlers/amazon.js');

const all_verses = require('./data/all_verses.js');
const study_verses = require('./data/study_verses.js');

const handlers = {
  'LaunchRequest': function () {
    console.log('LaunchRequest()->Bhagavad-gita As It Is Session Launched');
    let speechOutput = this.t('WELCOME_MESSAGE', this.t('SKILL_NAME'));

    this.attributes.speechOutput = speechOutput;
    this.attributes.repromptSpeech = this.t('WELCOME_REPROMPT');

    this.response.speak(this.attributes.speechOutput)
                 .listen(this.attributes.repromptSpeech);
    console.log('this.attributes.speechOutput: ' + this.attributes.speechOutput );
    this.emit(':responseReady');
  },

  'GetVerseIntent': function () {
    console.log('GetVerseIntent()');
    let speechOutput = 'Your verse for today is: ';
        speechOutput += getRandomVerse();

    console.log('speechOutput(): ' + speechOutput );

    this.attributes.speechOutput = speechOutput;
    this.response.speak(this.attributes.speechOutput);
    this.emit(':responseReady');
  },

  'GetStudyVerseIntent': function () {
    console.log('GetStudyVerseIntent()');
    let speechOutput = 'Here\'s a verse from the Bhakti Shastri Exam: ';
        speechOutput += getRandomStudyVerse();

    console.log('speechOutput: ' + speechOutput );

    this.attributes.speechOutput = speechOutput;
    this.response.speak(this.attributes.speechOutput);
                 //.shouldEndSession = true;
    this.emit(':responseReady');
  },


};

//  helper functions  ===================================================================
function getRandomVerse() {
  let newVerse = randomPhrase(all_verses.verses);
  return newVerse;
}

function getRandomStudyVerse() {
  let newVerse = randomPhrase(study_verses.verses);
  return newVerse;
}

function randomPhrase(array) {
  let i = 0;
  i = Math.floor(Math.random() * array.length);
  return(array[i]);
}

exports.handler = function(event, context, callback) {
  let alexa = Alexa.handler(event, context, callback);
  alexa.appId = 'amzn1.ask.skill.640a95ca-25d0-45fb-97b0-e5e9ba1a154e';
  alexa.resources = languageStrings;
  alexa.registerHandlers(handlers,
                         amazon_handlers
                        );
  alexa.execute();
};

