### Parse Bhagavad-gita

# Start on main Bhagavad-gita page:
  name = "vedabase.io"
  allowed_domains = [u'vedabase.io']
  start_urls = [u'https://vedabase.io/en/library/bg/']

  # extract links to each chapter:
  rules = [
      Rule(
          LinkExtractor(
              allow=(u'https://vedabase.io/en/library/bg/\d+'),
              deny=()
          ),
          callback='parse_item',
          follow=True
      )
  ]
  chapters = [
    "https://vedabase.io/en/library/bg/1/",
    "https://vedabase.io/en/library/bg/2/",
    "https://vedabase.io/en/library/bg/3/",
    "..."
  ]

  # visit each chapter page and retrieve list of verses:
  verses = [
    "https://vedabase.io/en/library/bg/1/1/",
    "https://vedabase.io/en/library/bg/1/2/",
    "https://vedabase.io/en/library/bg/1/3/",
    "https://vedabase.io/en/library/bg/1/4/",
    "https://vedabase.io/en/library/bg/1/5/",
    "..."
  ]


  # visit each page and extract:
  """
  verse #
  devanagri
  text
  synonyms
  translation
  purport
  """
  items = [
    [
      Item(PortiaItem, None, u'#content > .col-12',
        [
          Field(u'Verse_#', '.r > h1 *::text', []),
          Field(u'Devanagri', '.wrapper-devanagari > .r *::text', []),
          Field(u'sanskrit', '.wrapper-verse-text > .r *::text', []),
          Field(u'synonyms', '.wrapper-synonyms > .r > p *::text', []),
          Field(u'translation', '.wrapper-translation > .r > p *::text', []),
          Field(u'purport', '.wrapper-puport *::text', [])
        ]
      )
    ]
  ]

