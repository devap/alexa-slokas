# -*- coding: utf-8 -*-

import sys
import hashlib
from scrapy.exceptions import DropItem
from scrapy.http import Request
import mysql.connector
from mysql.connector import errorcode


# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


class BgPipeline(object):
  def process_item(self, item, spider):
    return item


class MySQLPipeline(object):

  def __init__(self, crawler):
    self.settings = crawler.settings
    self.dbname = self.settings.get('MYSQL_DB', None)
    self.dbhost = self.settings.get('MYSQL_HOST', 'localhost')
    self.dbport = self.settings.get('MYSQL_PORT', 3306)
    self.dbuser = self.settings.get('MYSQL_USER', None)
    self.dbpass = self.settings.get('MYSQL_PASSWORD', '')
    self.dbtable = self.settings.get('MYSQL_TABLE', None)



  @classmethod
  def from_crawler(cls, crawler):
      return cls(crawler)


  def process_item(self, item, spider):

    try:
      cnx = mysql.connector.connect(user=self.dbuser, password=self.dbpass,
                                    host=self.dbhost, database=self.dbname)

      cursor = cnx.cursor()

      add_verse = ("INSERT INTO slokas (verse_id, translation, url) "
                   "VALUES (%s, %s, %s)")

      data_verse = (item['verse_id'], item['translation'], item['url'])

      # Insert new verse
      cursor.execute(add_verse, data_verse)

      # Make sure data is committed to the database
      cnx.commit()

    except mysql.connector.Error as err:

      if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
      elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
      else:
        print(err)
    else:

      cursor.close()
      cnx.close()

