# -*- coding: utf-8 -*-

import re
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader
from bg.items import BgItem


#class BgversesSpider(scrapy.Spider):
class BgversesSpider(CrawlSpider):
  name = "bgverses"
  allowed_domains = ['vedabase.io']
  start_urls = ['https://vedabase.io/en/library/bg']

  def remove_prefix(self, prefix, text):
    if text.startswith(prefix):
      return text[len(prefix):]
    return text

  def parse(self, response):

    SET_SELECTOR = '#content'
    #SET_SELECTOR = '.r-chapter'
    for chapter in response.css(SET_SELECTOR):

      CHAPTER_NAME = '.r-chapter > a ::text'
      CHAPTER_LINK = '.r-chapter > a ::attr(href)'

      chapters = chapter.css(CHAPTER_NAME).extract()
      links = chapter.css(CHAPTER_LINK).extract()

      chapter_links = []

      # regex to determine which links to extract:
      regex = '/en/library/bg/\d+'
      for idx, link in enumerate(links):
        if re.match(regex, link) is not None:
          #chapter_links.append({'name': chapters[idx], 'link': link })
          yield scrapy.Request(response.urljoin(link), callback=self.parse_chapter )

  def parse_chapter(self, response):
    print('parse_chapter()Processing..' + response.url)
    verse_links = response.css('.rich-text dt a::attr(href)').extract()
    for verse in verse_links:
      #yield scrapy.Request(a, callback=self.parse_verse)
      print ('verse_link: ', verse)
      yield scrapy.Request(response.urljoin(verse), callback=self.parse_verse)


  def parse_verse(self, response):
    print('parse_verse()Processing..' + response.url)

    verse_id = response.css('.r-verse h1::text').extract()
    verseOrig = self.remove_prefix('Bg. ', verse_id[0])
    chapter, verse = verseOrig.split('.')

    devanagri = response.css('.r-devanagari *::text').extract()
    sanskrit = response.css('.r-verse-text *::text').extract()
    synonyms = response.css('.r-synonyms > p *::text').extract()
    translation = response.css('.r-translation > p *::text').extract()
    purport = response.css('.wrapper-puport *::text').extract()

    newverse = BgItem()
    newverse['chapter'] = chapter
    newverse['verse'] = verse
    newverse['devanagri'] = devanagri
    newverse['sanskrit'] = sanskrit
    newverse['synonyms'] = synonyms
    newverse['translation'] = translation
    newverse['purport'] = purport
    newverse['url'] = response.url

    yield newverse




