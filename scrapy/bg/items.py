# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BgItem(scrapy.Item):

  chapter = scrapy.Field()
  verse = scrapy.Field()
  devanagri = scrapy.Field()
  sanskrit = scrapy.Field()
  synonyms = scrapy.Field()
  translation = scrapy.Field()
  purport = scrapy.Field()
  url = scrapy.Field()
